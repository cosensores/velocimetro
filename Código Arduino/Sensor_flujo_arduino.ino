const int sensorPin = 2;
const int measureInterval = 2500;
volatile int pulseConter;
int medir=0;

// FS300A
const float factorK = 5.5;


void ISRCountPulse()
{
  pulseConter++;
}

float GetFrequency()
{
  pulseConter = 0;

  interrupts();
  delay(measureInterval);
  noInterrupts();

  return (float)pulseConter * 1000 / measureInterval;
}

int GetMetodo()
{ int seleccionMenu = Serial.parseInt();

  while (Serial.available() == 0) {
} 

  switch (seleccionMenu) {
    case 1:
      // profundidad menor a 1m 
      Serial.println("Usted debe usar el método de los 2 puntos");
      return 1;
    
    case 2:
      // profundidad menor a 1m 
      Serial.println("Usted debe usar el método de los 3 puntos");
      return 2;

    default:
      return 0;
      break;
  }
}
/* -------------------------------------------------------------------------------------------------------*/
float GetSubsecciones()
{ 
  int largodelrio = Serial.parseInt();
  while (Serial.available() == 0) {
}   
  return largodelrio;
}

void setup()
{
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(sensorPin), ISRCountPulse, RISING);

}

void loop()
{
  Serial.println("Si la profundidad del cuerpo de agua es MENOR a un metro presione 1, si es MAYOR a un metro presione 2.");
  
  int metodo=GetMetodo();
  
 while (metodo == 0){
    metodo=GetMetodo();
    break;
 } 
  
 while (metodo != 0){  
    Serial.println("Introduzca la longitud del cuerpo de agua en metros");
    float longitudrio = GetSubsecciones();
    
    while (longitudrio == 0){
      longitudrio = GetSubsecciones();
      break;    
    } 
   
     while (longitudrio != 0)  { 
        float longitudsegmento = longitudrio/10;
        Serial.print("RECOMENDADO medir cada: ");
        Serial.print(longitudsegmento);
        Serial.println(" metros.");
        medir = 1;
        break;
  } 
  break;
 }

while (medir == 1){
   // obtener frecuencia en Hz
  float frequency = GetFrequency();
   // calculos
  float flow_Lmin = frequency / factorK;
  float area = (0.75*0.254/2)*(0.75*0.254/2)*3.14;
  float velocidad = flow_Lmin / area;

  Serial.print("caudal: ");
  Serial.print(flow_Lmin, 3);
  Serial.println(" (L/min)");
  Serial.print("velocidad: ");
  Serial.print(velocidad, 3);
  Serial.println(" (dm/min)");
 
  }
}