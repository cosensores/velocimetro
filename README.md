# CAUDALÍMETRO
**Herramienta libre para la determinación de caudal de cursos de agua (canales y arroyos superficiales).**
 
> * Adaptación del código tomado de [Luis Llamas](https://www.luisllamas.es/caudal-consumo-de-agua-con-arduino-y-caudalimetro/)
> * La herramienta está basada en el Sensor de Flujo FS300A (3/4")
> * Existen múltiples métodos para determinar caudales. En este caso se presenta uno de ellos.

# Caudal
El caudal es la cantidad de agua que lleva una corriente en una unidad de tiempo. En el caso que la corriente presente una superficie libre a la atmósfera se la considera un canal abierto. En el caso de los canales abiertos, el flujo incluye no solo el agua sino cualquier sedimento, sólido suspendido y/o sólido disuelto que se encuentre en la corriente.

El caudal depende de la velocidad con la que fluye el agua y la sección transversal de la corriente (la profundidad y el ancho).

¿Qué análisis podemos hacer con el caudal?
> * Permite realizar un balance hídrico de un determinado sistema. El balance permite calcular el caudal total de un curso de agua a partir de la suma de los caudales parciales de los afluentes/aportantes.

        $B_{T} = \sum_{i=1}^{n}B_{Total}=B_{1}+ ... + B_{n}$

> * Permite determinar los flujos másicos de cualquier sustancia presente en la columna de agua: 

        \[
        Flujo_{másico} = Concentración * Caudal
        \]

¿Cuál es el procedimiento para medir el caudal en un cuerpo de agua?
1. Elegir la sección del curso de agua para realizar el aforo (la medición) en función de las siguientes características:
> * El curso de agua debe ser recto aguas abajo y aguas arriba de la sección por al menos 3 veces el ancho del canal.
> * La dirección del flujo debe ser paralela a los márgenes.
> * El lecho debe estar libre de grandes rocas, muelles, vegetación o cualquier otro obstáculo que pueda causar turbulencia o crear una componente vertical de velocidad.
> * La velocidad del agua y la profundidad máxima de la sección deben ser consistentes con las capacidades del equipo de medición utilizado.

NOTA: Estas consideraciones son planteadas para un caso ideal, pero podemos adaptarlas a en función de las posibilidades y restricciones de nuestro caso de estudio. Steniendo en cuenta que esto genera limitaciones en el alcance del dato.

2. Determinar el número de segmentos en el que vamos a dividir la sección transversal de aforo.
> * Criterio: no más del 10% del caudal debe pasar por un segmento. Como recomendación, que el ancho de la subsección sea calculado como: 
        \[
        $$Ancho_{s}=Ancho_{t}/10$$
        \]
Donde $Ancho_{s}$ es el ancho de la subsección y $Ancho_t$ es el ancho total. 
> * A mayor número de segmentos, mayor será la precisión de la medición (es decir, disminuye la incertidumbre).
> * El número de puntos de medición de velocidad por cada vertical depende de la profundidad de esa vertical:
> a. Cuando la profundidad sea mayor a 1 metro medir en 2 puntos (medir a 2 profundidades distintas).
> b. Cuando la profundidad sea menor o igual a 1 metro medir en 1 puntos (medir a 1 profundidad).

3. Cálculo:
a. Primero se calculan los caudales parciales ($Q_{i}$) de cada subsección:
$$ A_{i}= ancho_{i} * profundidad_{i}$$
$$ V_{i}= (velocidad_{1i}+velocidad_{2i})/2$$
$$Q_{i}= A_{i} * V_{i}$$
Donde $A_{i}$, $V_{i}$ y $Q_{i}$ son el área, la velocidad y el caudal de la subsección i respectivamente.

b. Luego, calcular el caudal total ($Q_{T}$) como la suma de caudales parciales:

$$ Q_{T}=sum_{k=1}^n Q_{i} $$ 
